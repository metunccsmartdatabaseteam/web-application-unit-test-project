﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SmartDatabaseWebApplication;
using SmartDatabaseWebApplication.Controllers;
using SmartDatabaseWebApplication.Classes;
using System.Threading.Tasks;

namespace SmartDatabaseTestProjecct
{
    [TestClass]
    public class UnitTest
    {
        /// <summary>
        /// This unit test function is used to test the login functions
        /// </summary>
        [TestMethod]
        public void LoginControlTests()
        {
            
            //Run existance task to see if the given e-mail address is registered or not
            var task = Task.Run(() => new RegistrationControl().IsRegisteredAsync("cinar.berkem@gmail.com"));
            //Compare the result with the expected result, in this case "true"
            Assert.AreEqual(true, task.Result);
            
            
            //Run login task to see if the given e-mail address and password is correct 
            var task2 = Task.Run(() => new RegistrationControl().IsPasswordCorrectAsync("cinar.berkem@gmail.com","1234567890"));
            //Compare the result with the expected result, in this case "true"
            Assert.AreEqual(true, task2.Result);



            //Run existance task to see if the given e-mail address is registered or not
            var task3 = Task.Run(() => new RegistrationControl().IsRegisteredAsync("some guy"));
            //Compare the result with the expected result, in this case "false"
            Assert.AreEqual(false, task3.Result);


            //Run login task to see if the given e-mail address and password is correct 
            var task4 = Task.Run(() => new RegistrationControl().IsPasswordCorrectAsync("cinar.berkem@gmail.com", "1234"));
            //Compare the result with the expected result, in this case "false"
            Assert.AreEqual(false, task4.Result);

        }

        /// <summary>
        /// This unit test function is used to test the registration tasks, including data encryption usage for both sided testing
        /// </summary>
        [TestMethod]
        public void SignUpControlTests()
        {

            //Create password encryption with salt
            var crypto = new SimpleCrypto.PBKDF2();
            var userSalt = crypto.GenerateSalt();
            //Run registiration task for given details
            var task = Task.Run(() => new RegistrationControl().AddFullUser("New Guy","newGuy@gmail.com",crypto.Compute("Iamnewguy"),"APositive","None","None","here is my photo", userSalt));
            //Compare the result with the expected result, in this case "true", saying error free operation
            Assert.AreEqual(true, task.Result);


            //Run login task to see if the registered user is really added 
            var task2 = Task.Run(() => new RegistrationControl().IsPasswordCorrectAsync("newGuy@gmail.com", "Iamnewguy"));
            //Compare the result with the expected result, in this case "true"
            Assert.AreEqual(true, task2.Result);


            //Create password encryption with salt
            var crypto2 = new SimpleCrypto.PBKDF2();
            var userSalt2 = crypto.GenerateSalt();
            //Run registiration task for same details to see primary key functioning
            var task3 = Task.Run(() => new RegistrationControl().AddFullUser("New Guy", "newGuy@gmail.com", crypto2.Compute("Iamnewguy"), "APositive", "None", "None", "here is my photo", userSalt2));
            //Compare the result with the expected result, in this case "false", stating error 
            Assert.AreEqual(false, task3.Result);


        }

         /// <summary>
         ///This unit test methods tests if the data coming from the database is correct or not
         /// </summary>
        [TestMethod]
        public void ProfileGatheringTests()
        {
            //Creating an user object with signup inputs
            SmartDatabaseWebApplication.Classes.LogicClasses.User user = new SmartDatabaseWebApplication.Classes.LogicClasses.User();
            user.name = "New Guy";
            user.mail = "newGuy@gmail.com";
            user.password = ""; //Since the data is encrypted one way, it cannot be compared with raw data
            user.bloodType = "APositive";
            user.allergies = "None".Split(';');
            user.chronics = "None".Split(';');
            user.photo = "here is my photo";


            //Getting the regarding user from the database
            var task = Task.Run(() => new CosmosConnection().GetUser("newGuy@gmail.com"));

            //Compare the results
            Assert.AreEqual(user.name, task.Result.name);
            Assert.AreEqual(user.mail, task.Result.mail);
            Assert.AreEqual(user.photo, task.Result.photo);
            Assert.AreEqual(user.bloodType, task.Result.bloodType);

            int i = 0;
            foreach(var x in user.allergies)
            {
                Assert.AreEqual(x, task.Result.allergies[i]);
                i++;
            }


            int j = 0;
            foreach (var x in user.chronics)
            {
                Assert.AreEqual(x, task.Result.chronics[j]);
                j++;
            }
        }

        /// <summary>
        /// This test is used to determine if the updates that are done on the accounts are correctly done or not
        /// </summary>
        [TestMethod]
        public void AccountUpdateTests()
        {
            //Using the account used on Signup and ProfileGathering tests
            //New blood type is given
            var task = Task.Run(() => new RegistrationControl().UpdateBloodType("newGuy@gmail.com","BPositive"));

            //Compare result, "true" means successful operation
            Assert.AreEqual(true, task.Result);


            //New photo is given (byte data is taken as string)
            var task2 = Task.Run(() => new RegistrationControl().UpdatePhoto("newGuy@gmail.com", "New Photo"));

            //Compare result, "true" means successful operation
            Assert.AreEqual(true, task2.Result);

            //New Allergy Entries are given for the user, seperated with ';' as incoming format 
            var task3 = Task.Run(() => new RegistrationControl().AddAllergyAsync("newGuy@gmail.com", "newAllergy1;newAllergy2"));

            //Compare result, "true" means successful operation
            Assert.AreEqual(true, task3.Result);

            //New Chronic Entries are given for the user, seperated with ';' as incoming format 
            var task4 = Task.Run(() => new RegistrationControl().AddChronicAsync("newGuy@gmail.com", "newChronic1;newChronic2"));

            //Compare result, "true" means successful operation
            Assert.AreEqual(true, task4.Result);


            ///Read the user from the database again and check results as in the ProfileGathering Test
            ///
            //Creating an user object with signup inputs
            SmartDatabaseWebApplication.Classes.LogicClasses.User user = new SmartDatabaseWebApplication.Classes.LogicClasses.User();
            user.name = "New Guy";
            user.mail = "newGuy@gmail.com";
            user.password = ""; //Since the data is encrypted one way, it cannot be compared with raw data
            user.bloodType = "BPositive";
            user.allergies = "newAllergy1;newAllergy2".Split(';');
            user.chronics = "newChronic1;newChronic2".Split(';');
            user.photo = "New Photo";


            //Getting the regarding user from the database
            var task5 = Task.Run(() => new CosmosConnection().GetUser("newGuy@gmail.com"));

            //Compare the results
            Assert.AreEqual(user.name, task5.Result.name);
            Assert.AreEqual(user.mail, task5.Result.mail);
            Assert.AreEqual(user.photo, task5.Result.photo);
            Assert.AreEqual(user.bloodType, task5.Result.bloodType);

            int i = 0;
            foreach (var x in user.allergies)
            {
                Assert.AreEqual(x, task5.Result.allergies[i]);
                i++;
            }


            int j = 0;
            foreach (var x in user.chronics)
            {
                Assert.AreEqual(x, task5.Result.chronics[j]);
                j++;
            }

        }

        /// <summary>
        /// This unit test method is to test update operations of individual MAC and individual location information of the users
        /// </summary>
        [TestMethod]
        public void IndividualLocationAndMACTests()
        {
            //Adding location to regarding to user
            var task = Task.Run(() => new LocationAndDisasterControl().AddUserLocationAsync("newGuy@gmail.com","35.60","33.20","02;04;18;7;00;15","70"));

            //Comparing result as true means there was no problem during the process
            Assert.AreEqual(true, task.Result);

            //Adding location and MAC address to regarding to user
            var task2 = Task.Run(() => new LocationAndDisasterControl().AddUserLocationAndMacAsync("newGuy@gmail.com", "35.60", "33.20","Some MAC Address","02;04;18;7;00;15", "70"));

            //Comparing result as true means there was no problem during the process
            Assert.AreEqual(true, task2.Result);


            //Reading user from database to see if the location and MAC info is correctly gather and read
            //Getting the regarding user from the database
            var task3 = Task.Run(() => new CosmosConnection().GetUser("newGuy@gmail.com"));

            ///Comparing results 
            Assert.AreEqual("35.6", task3.Result.location.latitude.ToString());
            Assert.AreEqual("33.2", task3.Result.location.longitude.ToString());
            Assert.AreEqual("Some MAC Address", task3.Result.MAC);
            Assert.AreEqual("2", task3.Result.location.time.Day.ToString());
            Assert.AreEqual("4", task3.Result.location.time.Month.ToString());
            Assert.AreEqual("18", task3.Result.location.time.Year.ToString());
            Assert.AreEqual("7", task3.Result.location.time.Hour.ToString());
            Assert.AreEqual("0", task3.Result.location.time.Minute.ToString());
            Assert.AreEqual("15", task3.Result.location.time.Second.ToString());
            Assert.AreEqual("70", task3.Result.heartBeat);



            //Changing MAC of regarding to user
            var task4 = Task.Run(() => new LocationAndDisasterControl().AddUserMacAsync("newGuy@gmail.com","New MAC Address"));


            //Comparing result as true means there was no problem during the process
            Assert.AreEqual(true, task4.Result);


            //Getting the regarding user from the database
            var task5 = Task.Run(() => new CosmosConnection().GetUser("newGuy@gmail.com"));


            //Comparing result for new MAC
            Assert.AreEqual("New MAC Address", task5.Result.MAC);


            //Adding new location and MAC address to regarding to user to see the updated data
            var task6 = Task.Run(() => new LocationAndDisasterControl().AddUserLocationAndMacAsync("newGuy@gmail.com", "35.40", "33.40", "Final MAC", "02;04;18;8;15;20", "67"));

            //Comparing result as true means there was no problem during the process
            Assert.AreEqual(true, task6.Result);


            //Getting the regarding user from the database
            var task7 = Task.Run(() => new CosmosConnection().GetUser("newGuy@gmail.com"));

            ///Comparing results 
            Assert.AreEqual("35.4", task7.Result.location.latitude.ToString());
            Assert.AreEqual("33.4", task7.Result.location.longitude.ToString());
            Assert.AreEqual("Final MAC", task7.Result.MAC);
            Assert.AreEqual("2", task7.Result.location.time.Day.ToString());
            Assert.AreEqual("4", task7.Result.location.time.Month.ToString());
            Assert.AreEqual("18", task7.Result.location.time.Year.ToString());
            Assert.AreEqual("8", task7.Result.location.time.Hour.ToString());
            Assert.AreEqual("15", task7.Result.location.time.Minute.ToString());
            Assert.AreEqual("20", task7.Result.location.time.Second.ToString());
            Assert.AreEqual("67", task7.Result.heartBeat);


        }


        /// <summary>
        /// This method is to test the nearby clients' location updates
        /// </summary>
        [TestMethod]
        public void NearbyLocationTests()
        {
            //Setting nearby clients with individual info
            var task = Task.Run(() => new LocationAndDisasterControl().SetNearbysAsync("newGuy@gmail.com","mac1;35.6;33.2|mac2;35.5;33.3|mac3;35.4;33.3", "02;04;18;8;15;20","65",float.Parse("35.60"),float.Parse("33.20")));

            //Comparing result as true means there was no problem during the process
            Assert.AreEqual(true, task.Result);


            //Getting the regarding user from the database
            var task2 = Task.Run(() => new CosmosConnection().GetUser("newGuy@gmail.com"));

            ///Comparing results 
            Assert.AreEqual("35.6", task2.Result.location.latitude.ToString("0.0"));
            Assert.AreEqual("33.2", task2.Result.location.longitude.ToString("0.0"));
            Assert.AreEqual("2", task2.Result.location.time.Day.ToString());
            Assert.AreEqual("4", task2.Result.location.time.Month.ToString());
            Assert.AreEqual("18", task2.Result.location.time.Year.ToString());
            Assert.AreEqual("8", task2.Result.location.time.Hour.ToString());
            Assert.AreEqual("15", task2.Result.location.time.Minute.ToString());
            Assert.AreEqual("20", task2.Result.location.time.Second.ToString());
            Assert.AreEqual("65", task2.Result.heartBeat);
            Assert.AreEqual("mac1;35.6;33.2", task2.Result.nearbys[0]);
            Assert.AreEqual("mac2;35.5;33.3", task2.Result.nearbys[1]);
            Assert.AreEqual("mac3;35.4;33.3", task2.Result.nearbys[2]);
        }

        /// <summary>
        /// This method creates an admin for the testing then generates a token for the regarding admin and sends it to his/her mail address.
        /// </summary>
        [TestMethod]
        public void AdminLoginWithTokenGenerationTests()
        {
            //Create admin for testing
            var task = new AdminControl().AddAdmin("newAdmin", "1234", "TestAdmin", "cinar.berkem@gmail.com");

            //Comparing result as true means there was no problem during the process
            Assert.AreEqual(true, task);


            //Check if the admin is created successfully
            var task2 = new AdminControl().IsAdminApproved("newAdmin","1234");

            //Comparing result as true means there was no problem during the process
            Assert.AreEqual(true, task2);


            //Generate token 
            var task3 = new AdminControl().CreateAdminToken("newAdmin", "1234");

            //Comparing result as true means there was no problem during the process
            Assert.AreEqual(true, task3);

        }

        /// <summary>
        /// This test is to see if the token is working within the expiration duration
        /// </summary>
        [TestMethod]
        public void TokenControlWithinTimeLimitTests()
        {
            //Check the incoming token
            var task = new AdminControl().CheckAdminToken("vqNFN1ew1YjYoriF9qxYR5OIpvbJMf/u");

            //Compare the result within time limit of 5 minutes, response should be true
            Assert.AreEqual(true, task);
        }


        /// <summary>
        /// This test is to see if the token is discarded after the expiration duration
        /// </summary>
        [TestMethod]
        public void TokenControlAfterExpirationTests()
        {
            //Check the incoming token
            var task = new AdminControl().CheckAdminToken("vqNFN1ew1YjYoriF9qxYR5OIpvbJMf/u");

            //Compare the result after time limit of 5 minutes, response should be false
            Assert.AreEqual(false, task);
        }

    }
}
